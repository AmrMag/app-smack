//
//  ChatVC.swift
//  App Smack
//
//  Created by amr on 2/22/18.
//  Copyright © 2018 amr. All rights reserved.
//

import UIKit

class ChatVC: UIViewController {

    // Outlets
    @IBOutlet weak var sideMenuView: UIView!
    @IBOutlet weak var sideMenuLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var menuBtn: UIButton!
    
    @IBOutlet weak var loginBtn: UIButton!
    
    //Variables and Constants
    var isSlideMenuHidden = true
    var menuWidth : CGFloat!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       menuWidth = sideMenuView.frame.width
        sideMenuLeadingConstraint.constant = 0 - menuWidth
        
        
    }

    //MARK: - IBActions functions
    //****************************************************
    
    
        //top burger menu pressed to reveal Side Menu
    
    @IBAction func loginBtnClicked(_ sender: Any) {
        performSegue(withIdentifier: TO_LOGIN, sender: nil)
    }
    
    
    
    @IBAction func menuBtnClicked(_ sender: UIButton) {
        if isSlideMenuHidden {
            sideMenuLeadingConstraint.constant = 0
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutIfNeeded()
            })
        }
        else {
            sideMenuLeadingConstraint.constant = 0 - menuWidth
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutIfNeeded()
            })
        }
        isSlideMenuHidden = !isSlideMenuHidden
    }
    
}
